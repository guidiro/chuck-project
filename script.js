const botaoPiada = document.querySelector("#novapiada");
const piada = document.querySelector("#piada");

function novaPiada(){ 
    fetch(`http://api.icndb.com/jokes/random/`).then(extrairJSON).then(preencherPiada);
}

function extrairJSON(resposta){
    return resposta.json();
}

function preencherPiada(dados){
    let piadas = dados.value.joke;
    piada.innerHTML = piadas;
  
}

botaoPiada.onclick = novaPiada;
